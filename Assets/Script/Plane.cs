﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plane : MonoBehaviour {

    //public float upwardForce;	//300-500 seems to be showing the desired result

    public float jumpSpeed; 
	private Rigidbody2D rb;
	private Animator anim;

	void Start(){
		rb = GetComponent<Rigidbody2D> ();
		anim = GetComponentInChildren<Animator> ();
	}

	void Update(){
		if(Input.GetKeyDown(KeyCode.Mouse0)){
            //Vector2 upMovement = new Vector2 (0, upwardForce);
            //rb.AddForce (upMovement);
            rb.velocity = new Vector2(rb.velocity.x, jumpSpeed);
			anim.SetTrigger ("UpwardForceTrigger");
		}
	}
}
