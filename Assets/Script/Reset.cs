﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LevelManager))]
public class Reset : MonoBehaviour {


	private string resetToLevel = "Game";
	private LevelManager levelManager;

	void Start(){
		levelManager = GetComponent<LevelManager> ();
	}

	void OnTriggerEnter2D(Collider2D col){
		GameObject obj = col.gameObject;

		if(obj.GetComponent<Plane>()){
			levelManager.LoadLevel (resetToLevel);
		}
	}
}
