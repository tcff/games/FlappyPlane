﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockFormationGenerator : MonoBehaviour {

	public GameObject RockFormation;
	public float seenEvery__Seconds;
	public float roofValue, floorValue;
	public GameObject containerObject;

	private float spawnsPerSecond = 0;

	// Update is called once per frame
	void Update () {

		if (isTimeToSpawn ()) {
			Spawn (RockFormation);
		}
	}

	bool isTimeToSpawn(){
		
//		float spawnsPerSecond = 1 /seenEvery__Seconds;
//
//		if(seenEvery__Seconds < Time.deltaTime){
//			Debug.LogWarning("Spawn rate capped by frame rate");
//		}
//
//		float threshold = spawnsPerSecond * Time.deltaTime;
//
//		float randomValue = Random.value;
//		print (randomValue);
//		return (randomValue < threshold);

		spawnsPerSecond +=  Time.deltaTime;

		if (spawnsPerSecond > seenEvery__Seconds) {
			spawnsPerSecond = 0;
			return true;
		}
		return false;

	}

	void Spawn(GameObject obj){
		
		//random height generation of RockFormation
		float formationHeight = Random.Range (floorValue, roofValue);
		Vector2 formationPosition = new Vector2 (transform.position.x, formationHeight);	//conversion into vector

		//Instantiating the RockFormation on to the game
		GameObject RockFormations = Instantiate (obj, formationPosition, Quaternion.identity) as GameObject;
		RockFormations.transform.parent = containerObject.transform;
	}
}